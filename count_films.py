# coding: utf-8

import argparse
import random
import csv
from parse_file import get_genre, get_year
from parse_html import get_image, get_rating, get_wiki_reference


FILE_NAME = 'movies.csv'

genres = ['unknown', 'action', 'adventure', 'animation',
          "childrens's", 'comedy', 'crime',
          'documentary', 'drama', 'fantasy', 'film-Noir',
          'horror', 'musical', 'mystery',
          'romance', 'sci-fi', 'thriller', 'war', 'wastern']


def get_argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('--year', type=int, help='input year of film')
    parser.add_argument('--genre', type=str, help='input genre of film')
    try:
        args = parser.parse_args()
        year = args.year
        genre = args.genre
    except ValueError:
        print('Incorrect')
        exit()
    except IndexError:
        print('Format')
        exit()
    return [year, genre]


def films_by_genre_year(year, genre, readers):
    """ возвращат список фильмов по указаному году и жанру """
    result = None
    films = []
    gener_int = genres.index(genre)
    for row in readers:
        if len(row) > 1:
            row = [''.join(row)]
        split_row = row[0].split('|')
        film_genres = get_genre(split_row)
        film_year = get_year(split_row)
        if int(film_year) == int(year):
            if gener_int in film_genres:
                films.append(split_row[1])
    return films


def get_random_film(films, year):
    """
    возвращает имя, рейтинг, постер и
    ссылку случайного фильма из списка
    """
    if films:
        name = random.choice(films).split('(')[0]
        wiki_page = get_wiki_reference(name, year)
        result = [name, get_rating(name), get_image(name), wiki_page]
    else:
        result = None
    return result


if __name__ == '__main__':
    year, genre = get_argument()
    genre = genre.lower()
    with open(FILE_NAME, 'rb') as f:
        reader = csv.reader(f)
        films = films_by_genre_year(year, genre, reader)
        result = get_random_film(films, year)
    if not result:
        print 'No that films'
    print ('number of films is %s, film: %s rating = %s' %
              (len(films), result[0], result[1]))
