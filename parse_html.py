# coding: utf-8

import omdb
import wikipedia


def get_wiki_reference(name, year):
    request = 'incategory:"%s films" intitle:"%s"' % (year, name)
    page = wikipedia.page(request)
    return page.url


def get_image(name):
    """ Полуение постера """
    res = omdb.get(title=name)
    try:
        image = res['poster']
    except KeyError:
        image = None
    return image


def get_rating(name):
    """ Получение рейтинга фильма """
    res = omdb.get(title=name)
    try:
        rating = res['imdb_rating']
    except KeyError:
        rating = 'unknown'
    return rating
