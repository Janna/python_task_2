
TITLE_TAG = 'h1'
TEXT_TAG = 'b'


def get_html_string(string, tag):
    return '<%s>%s</%s>' % (tag, string, tag)


def get_html_poster(imgg, alt_text):
    return '<img src=%s alt="%s">' % (imgg, alt_text)


def get_reference_html(reference, text):
    return '<a href="%s">%s</a>' % (reference, text)


def create_html(name, rat, img, ref):
    title = get_html_string(name, TITLE_TAG)
    rating = get_html_string(rat, TEXT_TAG)
    if not img:
        poster = get_html_string('No poster of film', TEXT_TAG)
    else:
        poster = get_html_poster(img, 'This is poster of film')
    reference = get_reference_html(ref, 'WIKI')
    return '%s\n %s\n \n%s\n %s\n' % (title, poster, rating, reference)
