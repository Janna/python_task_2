import csv
from bottle import route, run, error
from count_films import films_by_genre_year, get_random_film
from make_html import create_html, get_html_string, TITLE_TAG


FILE_NAME = 'movies.csv'


@route('/movies/<genre>/<year>/')
def make_web(genre, year):
    with open(FILE_NAME, 'rb') as f:
        reader = csv.reader(f)
        genre = genre.lower()
        films = films_by_genre_year(year, genre, reader)
        result = get_random_film(films, year)
    if result:
        return create_html(result[0], result[1], result[2], result[3])
    return '<b>No that film<b>'


@error(404)
def error404(error):
    return get_html_string('ERROR 404', TITLE_TAG)


if __name__ == '__main__':
    run(host='localhost', port=8080)
