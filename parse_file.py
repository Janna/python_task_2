# coding: utf-8

import csv


def get_genre(list):
    COUNT_GENERS = 19
    film_geners = list[-COUNT_GENERS:len(list)]
    return [i for i in range(len(film_geners)) if film_geners[i] == '1']


def get_year(list):
    LENGHT_YEAR = 5
    data = list[1][-LENGHT_YEAR:-1]
    try:
        result = int(data)
    except ValueError:
        result = -1
    return result
